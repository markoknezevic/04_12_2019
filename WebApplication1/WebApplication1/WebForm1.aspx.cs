﻿using BusinesLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        private StudentBusines studentBusines;
        protected void Page_Load(object sender, EventArgs e)
        {
            RefreshStudnetList();
        }
        private void RefreshStudnetList()
        {
            this.studentBusines = new StudentBusines();
            List<Student> studentList = studentBusines.getAllStudents();
            ListBox1.Items.Clear();

            foreach (Student student in studentList)
            {
                ListBox1.Items.Add(student.Name + " " + student.Surname);
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            InsertStudent();
        }

        public void InsertStudent()
        {
            Student student = new Student(Convert.ToInt32(TextBoxId.Text) ,TextBoxName.Text, TextBoxSurname.Text, TextBoxIndex.Text);
            this.studentBusines.InsertStudent(student);
            RefreshStudnetList();
        }
    }
}