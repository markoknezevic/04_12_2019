﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinesLayer
{
    public class StudentBusines
    {
        private StudentRepository studentRepository;

        public StudentBusines()
        {
            this.studentRepository = new StudentRepository();
        }
        public List<Student> getAllStudents()
        {
            return studentRepository.getAllStudents();
        }

        public List<Student> getStudentsByName(string name)
        {
            return studentRepository.getAllStudents().Where(s => s.Name=="ddd").ToList();
        }

        public string InsertStudent(Student student)
        {
            int rows = this.studentRepository.InsertStudent(student);

            if(rows > 0)
            {
                return "Success";
            }
            else
            {
                return "Fail";
            }
        }
    }
}
