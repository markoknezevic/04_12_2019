﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Student
    {
        private int id;
        private String name;
        private String surname;
        private String index;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Index { get => index; set => index = value; }

        public Student(int id, string name, string surname, string index)
        {
            this.Id = id;
            this.Name = name;
            this.Surname = surname;
            this.Index = index;
        }
    }
}
